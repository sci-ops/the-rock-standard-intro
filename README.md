# The ROCK standard

### The Reproducible Open Coding Kit: A Human And Machine Readable Standard For Coding Qualitative Data

The rendered article is available at https://sci-ops.gitlab.io/the-rock-standard-intro as a website and at https://sci-ops.gitlab.io/the-rock-standard-intro/peters--zorgo---2023---the-rock-standard---manuscript.pdf as a PDF document.

<!-- Note that the preprint is located at https://doi.org/10.31234/osf.io/8tpcv. -->
